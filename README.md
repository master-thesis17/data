# General Description

Data of 73 stocks listed in NASDAQ. 
The list of stocks is picked by random sampling.
They include several stocks that skyrocket: increase by 100\% or more in value within a week.

This data is collected for my master thesis, where I predict stocks that increase by 100\% over 3 months.

# Project Structure

hist contains price history\
reddits contains reddit messages
